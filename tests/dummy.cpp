
#include <gtest/gtest.h>

class GroupAlfa : public::testing::Test {
public:
    GroupAlfa() { /* init protected members here */ }
    ~GroupAlfa() { /* free protected members here */ }
protected:
    /* none yet */
};

TEST_F(GroupAlfa, test1) {  ASSERT_EQ(1, 1); }
TEST_F(GroupAlfa, test2) {  ASSERT_EQ(1, 2); }
//TEST_F(GroupAlfa, test3) {  ASSERT_EQ(1, 1); }
//TEST_F(GroupAlfa, test4) {  ASSERT_EQ(1, 1); }
//TEST_F(GroupAlfa, test5) {  ASSERT_EQ(1, 1); }
//TEST_F(GroupAlfa, test6) {  ASSERT_EQ(1, 1); }
//TEST_F(GroupAlfa, test7) {  ASSERT_EQ(1, 1); }
//TEST_F(GroupAlfa, test8) {  ASSERT_EQ(1, 1); }

class GroupBetta : public::testing::Test {
public:
    GroupBetta() { /* init protected members here */ }
    ~GroupBetta() { /* free protected members here */ }
protected:
    /* none yet */
};

TEST_F(GroupBetta, testA) {  ASSERT_EQ(1, 1); }
//TEST_F(GroupBetta, testB) {  ASSERT_EQ(1, 1); }
//TEST_F(GroupBetta, testC) {  ASSERT_EQ(1, 1); }
//TEST_F(GroupBetta, testX) {  ASSERT_EQ(1, 1); }
//TEST_F(GroupBetta, testY) {  ASSERT_EQ(1, 1); }
//TEST_F(GroupBetta, testZ) {  ASSERT_EQ(1, 1); }