#!/bin/bash
clear
echo "******************* start regressin script *************************"

SOURSE_TEST_FILE=$1
PASSED_TEST_FILE=$2 
FULL_TEST_FILE=$3 
echo  $1 $2 $3
echo  $SOURSE_TEST_FILE $PASSED_TEST_FILE $FULL_TEST_FILE

#SOURSE_TEST_FILE="./tests/Debug/tests"
#PASSED_TEST_FILE="./passed_test.txt"
RESULT_SINGLE_TEST="./testresult.xml"

IFS=$'\n'
EXIT_CODE=0

if [ -e $PASSED_TEST_FILE ]
then
 echo $PASSED_TEST_FILE Exist!
else
 echo Create emty $PASSED_TEST_FILE! 
 echo "Passed tests list" >> $PASSED_TEST_FILE
 #echo "./tests/Debug/tests --gtest_filter=GroupAlfa.test2" >> $PASSED_TEST_FILE
fi

if [ -e $FULL_TEST_FILE ]
then
 echo "Remove old test list $FULL_TEST_FILE"
 rm $FULL_TEST_FILE
fi

$SOURSE_TEST_FILE --gtest_list_tests > $FULL_TEST_FILE

group_name="xxx";
for var in $(cat $FULL_TEST_FILE)
do
  tmp_line="$(echo -e "${var}" | sed -e 's/^[[:space:]]*//')"
  last_char="${tmp_line: -1}"
  if [ "$last_char" = "." ]
     then 
        echo "GROUP" $tmp_line 
        group_name=$tmp_line 
        continue
  fi
  if [ "$group_name" = "xxx" ]
     then 
        continue
  fi

  test_name="$tmp_line"
  if [ "$test_name" = "End test!" ]
     then
        continue
  fi
  exepath="$SOURSE_TEST_FILE --gtest_filter=$group_name$test_name"
#  echo $exepath 
  eval  "$exepath --gtest_output=xml:$RESULT_SINGLE_TEST" >> stdnull
  if grep -q 'failures="1"' $RESULT_SINGLE_TEST
     then
        echo "ERROR in" $exepath
        if grep -q $exepath $PASSED_TEST_FILE 
           then
              echo "REGRESSION!!!"
              EXIT_CODE=1
        fi
     else
        echo "OK!   in" $exepath
        grep -qF -- "$exepath" "$PASSED_TEST_FILE" || echo "$exepath" >> "$PASSED_TEST_FILE"
  fi
done
#exit $EXIT_CODE
exit 0